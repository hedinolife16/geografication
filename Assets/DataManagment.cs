﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManagment : MonoBehaviour
{
    public static DataManagment dataManagment;

    public int highScore;

    void Awake()
    {
        if (dataManagment == null)
        {
            DontDestroyOnLoad(gameObject);
            dataManagment = this;
        }
        else if (dataManagment != this)
            Destroy(gameObject);
    }

    public void SaveData()
    {
        //Data is Saved
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gameInfo.dat");
        GameData data = new GameData(); //Creates container for data
        data.highScore = this.highScore;
        binaryFormatter.Serialize(file, data); //serializes
        file.Close();
    }

    public void LoadData()
    {
        //Data is Loaded
        if (File.Exists(Application.persistentDataPath + "/gameInfo.dat"))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gameInfo.dat", FileMode.Open);
            GameData data = (GameData)binaryFormatter.Deserialize(file);
            file.Close();
            this.highScore = data.highScore;
        }
    }

    public void ClearData()
    {
        if (File.Exists(Application.persistentDataPath + "/gameInfo.dat"))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/gameInfo.dat");
            GameData data = new GameData(); //Creates container for data
            data.highScore = 0; //Resets to 0
            binaryFormatter.Serialize(file, data); //serializes
            file.Close();
        }
    }
}

[Serializable]
class GameData
{
    public int highScore;
}
