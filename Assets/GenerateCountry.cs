﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GenerateCountry : MonoBehaviour
{

    public string randomCountry;
    public string cryptCountry;
    public Text countryUI;
    public int countryNumber;
    public Text countryNumberUI;

    public Text answer;
    public InputField answerField;

    public float timeLeft = 121f;
    public int score = 0;
    public Text highscoreUI;
    public Text timeLeftUI;
    public Text scoreUI;
    public static bool levelDone = false;

    public GameObject loadData;

    public AudioSource audio;

    public Text resultatUI;

    void Start()
    {
        levelDone = false;

        countryNumber = 1;

        randomCountry = Countries();
        print(randomCountry);
        cryptCountry = CryptedCountry(randomCountry);
        print(cryptCountry);
        countryUI.text = "Nom du pays: " + cryptCountry;

        DataManagment.dataManagment.LoadData();
        highscoreUI.text += loadData.GetComponent<DataManagment>().highScore.ToString();
    }

    void Update()
    {
        if (levelDone == false)
            timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
        {
            levelDone = true;
            countryUI.text = "Temps ecoulé";
            answerField.enabled = false;
        }
            

        timeLeftUI.text = "Time Left : " + (int)timeLeft;
        scoreUI.text = "Score : " + score;

        if (countryNumber > 10)
        {
            DataManagment.dataManagment.highScore = score;
            DataManagment.dataManagment.SaveData();
            answerField.enabled = false;
            countryUI.text = "Terminer !";
            countryNumberUI.text = "10";
            
            levelDone = true;
        }

        if (Input.GetKeyUp(KeyCode.Slash))
            SceneManager.LoadScene("Game");
        else if (Input.GetKeyUp(KeyCode.Plus))
            Application.Quit();
    }

    public void Scoring()
    {

        countryNumber++;
        countryNumberUI.text = countryNumber.ToString();

        if (answerField.textComponent.GetComponent<Text>().text.ToLower() == randomCountry.ToLower())
        {
            Debug.Log("Data Says: highscore is currently : " + DataManagment.dataManagment.highScore);
            score += (int)timeLeft * 10;
            randomCountry = Countries();
            cryptCountry = CryptedCountry(randomCountry);
            countryUI.text = "Nom du pays: " + cryptCountry;
            resultatUI.text = "Correct";
            answerField.GetComponent<InputField>().text = "";

        }
        else
        {
            resultatUI.text = "C'était " + randomCountry;
            randomCountry = Countries();
            cryptCountry = CryptedCountry(randomCountry);
            countryUI.text = "Nom du pays: " + cryptCountry;
            answerField.GetComponent<InputField>().text = "";
        }
            

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ResetHighScore()
    {
        DataManagment.dataManagment.ClearData();
    }

    public void ResetGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void Typying()
    {
        audio.Play();
    }

    private string Countries()
    {

        System.Random random = new System.Random();

        string[] countries = {
            "Afghanistan",
            "Afrique du Sud",
            "Albanie",
            "Algérie",
            "Allemagne",
            "Andorre",
            "Angola",
            "Anguilla",
            "Antarctique",
            "Antigua-et-Barbuda",
            "Antilles néerlandaises",
            "Arabie saoudite",
            "Argentine",
            "Arménie",
            "Aruba",
            "Australie",
            "Autriche",
            "Azerbaïdjan",
            "Bahamas",
            "Bahreïn",
            "Bangladesh",
            "Barbade",
            "Bélarus",
            "Belgique",
            "Belize",
            "Bénin",
            "Bermudes",
            "Bhoutan",
            "Bolivie",
            "Bosnie-Herzégovine",
            "Botswana",
            "Brésil",
            "Brunéi Darussalam",
            "Bulgarie",
            "Burkina Faso",
            "Burundi",
            "Cambodge",
            "Cameroun",
            "Canada",
            "Cap-Vert",
            "Ceuta et Melilla",
            "Chili",
            "Chine",
            "Chypre",
            "Colombie",
            "Comores",
            "Congo-Brazzaville",
            "Corée du Nord",
            "Corée du Sud",
            "Costa Rica",
            "Côte d’Ivoire",
            "Croatie",
            "Cuba",
            "Danemark",
            "Diego Garcia",
            "Djibouti",
            "Dominique",
            "Égypte",
            "El Salvador",
            "Émirats arabes unis",
            "Équateur",
            "Érythrée",
            "Espagne",
            "Estonie",
            "Vatican",
            "Micronésie",
            "États-Unis",
            "Éthiopie",
            "Fidji",
            "Finlande",
            "France",
            "Gabon",
            "Gambie",
            "Géorgie",
            "Ghana",
            "Gibraltar",
            "Grèce",
            "Grenade",
            "Groenland",
            "Guadeloupe",
            "Guam",
            "Guatemala",
            "Guernesey",
            "Guinée",
            "Guinée équatoriale",
            "Guinée-Bissau",
            "Guyana",
            "Guyane française",
            "Haïti",
            "Honduras",
            "Hongrie",
            "Île Bouvet",
            "Île Christmas",
            "Île Clipperton",
            "Île de l'Ascension",
            "Île de Man",
            "Île Norfolk",
            "Îles Åland",
            "Îles Caïmans",
            "Îles Canaries",
            "Îles Cocos - Keeling",
            "Îles Cook",
            "Îles Féroé",
            "Îles Heard et MacDonald",
            "Îles Malouines",
            "Îles Mariannes du Nord",
            "Îles Marshall",
            "Îles Salomon",
            "Îles Turks et Caïques",
            "Îles Vierges",
            "Inde",
            "Indonésie",
            "Irak",
            "Iran",
            "Irlande",
            "Islande",
            "Italie",
            "Jamaïque",
            "Japon",
            "Jersey",
            "Jordanie",
            "Kazakhstan",
            "Kenya",
            "Kirghizistan",
            "Kiribati",
            "Koweït",
            "Laos",
            "Lesotho",
            "Lettonie",
            "Liban",
            "Libéria",
            "Libye",
            "Liechtenstein",
            "Lituanie",
            "Luxembourg",
            "Macédoine",
            "Madagascar",
            "Malaisie",
            "Malawi",
            "Maldives",
            "Mali",
            "Malte",
            "Maroc",
            "Martinique",
            "Maurice",
            "Mauritanie",
            "Mayotte",
            "Mexique",
            "Moldavie",
            "Monaco",
            "Mongolie",
            "Monténégro",
            "Montserrat",
            "Mozambique",
            "Myanmar",
            "Namibie",
            "Nauru",
            "Népal",
            "Nicaragua",
            "Niger",
            "Nigéria",
            "Niue",
            "Norvège",
            "Nouvelle-Calédonie",
            "Nouvelle-Zélande",
            "Oman",
            "Ouganda",
            "Ouzbékistan",
            "Pakistan",
            "Palaos",
            "Panama",
            "Papouasie-Nouvelle-Guinée",
            "Paraguay",
            "Pays-Bas",
            "Pérou",
            "Philippines",
            "Pitcairn",
            "Pologne",
            "Polynésie française",
            "Porto Rico",
            "Portugal",
            "Qatar",
            "régions éloignées de l’Océanie",
            "République centrafricaine",
            "République démocratique du Congo",
            "République dominicaine",
            "République tchèque",
            "Réunion",
            "Roumanie",
            "Royaume-Uni",
            "Russie",
            "Rwanda",
            "Sahara occidental",
            "Saint-Barthélémy",
            "Saint-Kitts-et-Nevis",
            "Saint-Marin",
            "Saint-Martin",
            "Saint-Pierre-et-Miquelon",
            "Saint-Vincent-et-les Grenadines",
            "Sainte-Hélène",
            "Sainte-Lucie",
            "Samoa",
            "Samoa américaines",
            "Sao Tomé-et-Principe",
            "Sénégal",
            "Serbie",
            "Serbie-et-Monténégro",
            "Seychelles",
            "Sierra Leone",
            "Singapour",
            "Slovaquie",
            "Slovénie",
            "Somalie",
            "Soudan",
            "Sri Lanka",
            "Suède",
            "Suisse",
            "Suriname",
            "Svalbard et Île Jan Mayen",
            "Swaziland",
            "Syrie",
            "Tadjikistan",
            "Taïwan",
            "Tanzanie",
            "Tchad",
            "Terres australes françaises",
            "Territoire britannique de l'océan Indien",
            "Thaïlande",
            "Timor oriental",
            "Togo",
            "Tokelau",
            "Tonga",
            "Trinité-et-Tobago",
            "Tristan da Cunha",
            "Tunisie",
            "Turkménistan",
            "Turquie",
            "Tuvalu",
            "Ukraine",
            "Uruguay",
            "Vanuatu",
            "Venezuela",
            "Viêt Nam",
            "Wallis-et-Futuna",
            "Yémen",
            "Zambie",
            "Zimbabwe"
        };

        int cIndex = random.Next(countries.Length);
        return countries[cIndex];

    }

    private string CryptedCountry(string randomCountry)
    {
        string cr = randomCountry;
        int l = cr.Length / 3;

        System.Random random = new System.Random();
        var rtnlist = new List<int>();

        for (int i = 0; i < l; i++)
        {
            rtnlist.Add(random.Next(0, cr.Length));
            print(rtnlist[i] + " " + cr[i]);
        }

        char[] crArray = cr.ToCharArray();
        foreach (int el in rtnlist)
        {
            if (crArray[el] != ' ' || crArray[el] != '-' || crArray[el] != '\'')
                crArray[el] = '*';
        }

        return cr = new string(crArray);
    }

}