﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Bienvenue : MonoBehaviour
{
    public AudioSource audio;

    private ArrayList welcomeList = new ArrayList(new[] {
        "Bienvenue",
        "ce test \n vous permettera",
        "d'approfondir vos \n connaissances en \n géographie",
        "Devinez les lettres manquantes \n des 10 pays qui vous \n seront proposés",
        "et gangez un maximum de POINTS",
        "Bonne chance !",
        "Pret ? \n (\"Espace\" pour commencer le jeu)"});
    private int wIndex;
    public Text text;

    // Start is called before the first frame update
    void Start()
    {
        wIndex = 0;
        text.text = welcomeList[0].ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Q))
            Application.Quit();
        else if (Input.GetKeyUp(KeyCode.X) || wIndex > 6)
        {
            SceneManager.LoadScene("Game");
            audio.Play();
        }
            
        else if (Input.GetKeyUp(KeyCode.Space) && wIndex <= 6)
        {
            audio.Play();
            wIndex++;
            text.text = welcomeList[wIndex].ToString();
        }

    }
}
